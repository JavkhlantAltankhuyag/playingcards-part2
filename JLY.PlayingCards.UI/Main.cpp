#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

// Rank enum
enum Rank
{
	TWO = 2, 
	THREE, 
	FOUR, 
	FIVE, 
	SIX, 
	SEVEN,
	EIGHT,
	NINE, 
	TEN,  
	JACK,
	QUEEN, 
	KING,
	ACE = 14

};

//Suit enum
enum Suit
{
	SPADE, 
	HEART, 
	DIAMOND, 
	CLUB
};

// Card Structure
struct Card
{
	// members
	Rank rank;
	Suit suit;
};

void PrintCard(Card card);
Card HighCard(Card card1, Card card2);

int main()
{
	_getch();
	return 0;
}

void PrintCard(Card card) {
	string rank;
	string suit;

	switch (card.rank) {
	case 2: rank = "Two";
		break;
	case 3: rank = "Three";
		break;
	case 4: rank = "Four";
		break;
	case 5: rank = "Five";
		break;
	case 6: rank = "Six";
		break;
	case 7: rank = "Seven";
		break;
	case 8: rank = "Eight";
		break;
	case 9: rank = "Nine";
		break;
	case 10: rank = "Ten";
		break;
	case 11: rank = "Jack";
		break;
	case 12: rank = "Queen";
		break;
	case 13: rank = "King";
		break;
	case 14: rank = "Ace";
		break;
	}

	switch (card.suit) {
	case 0: suit = "Spades";
		break;
	case 1: suit = "Hearts";
		break;
	case 2: suit = "Diamonds";
		break;
	case 3: suit = "Clubs";
		break;
	}

	cout << "The " << rank << " of " << suit << "\n";
}

Card HighCard(Card card1, Card card2) {
	if (card1.rank > card2.rank)
		return card1;
	else
		return card2;
}